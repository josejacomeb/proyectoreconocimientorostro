#!/usr/bin/env python3
'''
Pasos sudo apt-get install python3-pyqt5 libqt5gui5 libqtgui4 libqt4-test wiringpi libboost-all-dev && sudo pip3 install face_recognition opencv-python RPi.GPIO
configuracion RPi https://www.pyimagesearch.com/2017/05/01/install-dlib-raspberry-pi/
'''
from PyQt5.QtWidgets import QDialog
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from main import Ui_Dialog
import cv2
import numpy as np
import os
import platform
import time
if platform.machine() == 'armv7l':
    import RPi.GPIO as GPIO

import face_recognition #sudo pip install face_recognition

path = os.getcwd() + '/Personas/'
#import face_detection #Algoritmo detector rostros

cap = cv2.VideoCapture(0)
hasFrame,frame = cap.read()

class Control(QThread):
    def __init__(self, pin=11, tiempo=1, pinServo=17):
        QThread.__init__(self)
        self.pin = pin
        self.tiempo = tiempo
        self.pinServo = pinServo
        if platform.machine() == 'armv7l':
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(self.pinServo, GPIO.OUT)
            GPIO.setup(self.pin, GPIO.OUT) # Set LedPin's mode is output
            self.p = GPIO.PWM(self.pinServo, 50) # GPIO 17 for PWM with 50Hz
            self.p.ChangeDutyCycle(5)
    def run(self):
        if platform.machine() == 'armv7l':
            GPIO.output(self.pin, GPIO.HIGH) # Set LedPin high(+3.3V) to turn on led
            self.p.ChangeDutyCycle(12.5)
        time.sleep(self.tiempo)
        if platform.machine() == 'armv7l':
            GPIO.output(self.pin, GPIO.LOW) # Set LedPin high(+3.3V) to turn on led
            self.p.ChangeDutyCycle(0)
class Thread(QThread):
    changePixmap = pyqtSignal(QImage)
    nombres =  pyqtSignal(list)
    listanombre =  pyqtSignal(str)

    def __init__(self,a, cuadros=7):
        self.cuadros = cuadros
        self.carasreconocidas = []
        self.carascodificadas = []
        self.nombrecaras = []
        self.lista = ""
        QThread.__init__(self)
        files = []
        # r=root, d=directories, f = files
        for r, d, f in os.walk(path):
            for file in f:
                if '.jpg' in file or '.png' in file or '.jpeg' in  file:
                    files.append(os.path.join(r, file))
        for f in files:
            imagen = face_recognition.load_image_file(f)
            self.carascodificadas.append(face_recognition.face_encodings(imagen)[0])
            self.nombrecaras.append(os.path.splitext(os.path.basename(f ))[0])
            self.lista += os.path.splitext(os.path.basename(f ))[0] + ","
        # Initialize some variables
        self.face_locations = []
        self.face_encodings = []
        self.face_names = []
        self.process_this_frame = True
        self.contador = 0


    def run(self):
        print("Lista: " + self.lista)
        self.listanombre.emit(self.lista)
        while True:
            hasFrame,frame = cap.read()
            if not hasFrame:
                break
            if self.contador > self.cuadros:
                self.contador = 0;
            # Resize frame of video to 1/4 size for faster face recognition processing
            small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_small_frame = small_frame[:, :, ::-1]
            # Only process every other frame of video to save time
            self.face_locations = face_recognition.face_locations(rgb_small_frame)
            if self.contador == self.cuadros:
                self.face_encodings = face_recognition.face_encodings(rgb_small_frame, self.face_locations)

                self.face_names = []
                for face_encoding in self.face_encodings:
                    # See if the face is a match for the known face(s)
                    matches = face_recognition.compare_faces(self.carascodificadas, face_encoding)
                    name = "Desc."

                    # # If a match was found in self.carascodificadasdings, just use the first one.
                    # if True in matches:
                    #     first_match_index = matches.index(True)
                    #     name = self.nombrecaras[first_match_index]

                    # Or instead, use the known face with the smallest distance to the new face
                    face_distances = face_recognition.face_distance(self.carascodificadas, face_encoding)
                    best_match_index = np.argmin(face_distances)
                    if matches[best_match_index]:
                        name = self.nombrecaras[best_match_index]

                    self.face_names.append(name)

            # Display the results
            if self.contador == self.cuadros:
                for (top, right, bottom, left), name in zip(self.face_locations, self.face_names):
                    # Scale back up face locations since the frame we detected in was scaled to 1/4 size
                    top *= 4
                    right *= 4
                    bottom *= 4
                    left *= 4

                    # Draw a box around the face
                    cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

                    # Draw a label with a name below the face
                    cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
                    font = cv2.FONT_HERSHEY_DUPLEX
                    cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
            else:
                for (top, right, bottom, left) in self.face_locations:
                    # Scale back up face locations since the frame we detected in was scaled to 1/4 size
                    top *= 4
                    right *= 4
                    bottom *= 4
                    left *= 4

                    # Draw a box around the face
                    cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            convertToQtFormat = QImage(rgbImage.data, rgbImage.shape[1], rgbImage.shape[0], QImage.Format_RGB888)
            p = convertToQtFormat.scaled(270, 200, Qt.KeepAspectRatio)
            self.changePixmap.emit(p)
            if self.contador == self.cuadros:
                self.nombres.emit(self.face_names)
            self.contador += 1

class Ui_VentanaPrincipal(QDialog, Ui_Dialog):
    def __init__(self):
        super(Ui_VentanaPrincipal, self).__init__()
        # Set up the user interface from Designer.
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.th = Thread(VentanaPrincipal, cuadros = 2)
        self.th.changePixmap.connect(lambda p: self.ui.video.setPixmap(QPixmap(p)))
        self.th.nombres.connect(self.cambiar)
        self.th.listanombre.connect(self.inicializar)
        self.th.start()
        self.ui.Admitidas.setText(self.th.lista)
        self.threadControl = Control()
    def cambiar(self, nombres):
        cadena = ""
        abrir = False
        for nombre in nombres:
            if not "Desc." in nombre:
                abrir = True
                cadena += nombre + ","
            else:
                cadena += "Desconocido ,"
        if abrir:
            self.threadControl.start()
            if ~self.threadControl.isRunning():
                self.threadControl.start()
        if nombres:
            self.ui.nombreSujeto.setText(cadena)
    def inicializar(self, nombres):
        print(nombres)



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    VentanaPrincipal = QtWidgets.QMainWindow()
    ui = Ui_VentanaPrincipal()
    ui.show()
    sys.exit(app.exec_())
